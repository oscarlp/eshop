﻿using eShop.Catalog.Data.ContextDB;
using eShop.Catalog.Data.Factory;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Threading.Tasks;

namespace eShop.Catalog.Repositories
{
    public class RepositoryCatalog<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        private readonly IContextDBFactory dbContextFactory;
        public RepositoryCatalog(IContextDBFactory contextDBFactory)
        {
            this.dbContextFactory = contextDBFactory;
        }
        public async Task<bool> Create(TEntity entity)
        {
            if (dbContextFactory != null && entity != null)
            {
                try
                {
                    using (var context = dbContextFactory.Process())
                    {
                        context.DbSet<TEntity>().Add(entity);
                        await context.SaveChangesAsync();
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(ex.Message);
                }
            }
            else
            {
                throw new ArgumentException("Error. Context DB or entity cannot be null.");
            }
            return false;
        }

        public async Task<bool> Delete(TEntity entity)
        {
            if (dbContextFactory != null && entity != null)
            {
                try
                {
                    using (var context = dbContextFactory.Process())
                    {
                        context.DbSet<TEntity>().Remove(entity);//?
                        await context.SaveChangesAsync();
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(ex.Message);
                }
            }
            else
            {
                throw new ArgumentException("Error. Context DB or entity cannot be null.");
            }
            return false;
        }

        public async Task<int> ExecuteSP(DbCommand cmd)
        {
            int result = -1;
            try
            {
                if (cmd.Connection.State != ConnectionState.Open)
                {
                    cmd.Connection.Open();
                    var resp = await cmd.ExecuteScalarAsync();
                    result = (resp != null) ? Convert.ToInt32(resp) : -1;
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
            }
            return result;
        }

        /// <summary>
        /// Devuelve el objeto que corresponde a un identificador o valor clave específico.
        /// </summary>
        /// <param name="id">Valor clave identificador del objeto.</param>
        /// <returns>Objeto que corresponde al identificador especificado.</returns>
        public async Task<TEntity> Get(object id)
        {
            if (dbContextFactory != null)
            {
                try
                {
                    using (var context = dbContextFactory.Process())
                    {
                        return await context.DbSet<TEntity>().FindAsync(id);
                    }
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(ex.Message);
                }
            }
            else
            {
                throw new ArgumentException("Error. Context DB cannot be null.");
            }
            return null;
        }

        public async Task<List<TEntity>> List()
        {
            if (dbContextFactory != null)
            {
                try
                {
                    using (var context = dbContextFactory.Process())
                    {
                        return await context.DbSet<TEntity>().ToListAsync();
                    }
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(ex.Message);
                }
            }
            else
            {
                throw new ArgumentException("Error. Context DB cannot be null.");
            }
            return null;
        }

        public async Task<TEntity> Update(TEntity entity)
        {
            if (dbContextFactory != null && entity != null)
            {
                try
                {
                    using (var context = dbContextFactory.Process())
                    {
                        context.DbSet<TEntity>().Update(entity);
                        await context.SaveChangesAsync();
                        return entity;
                    }
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(ex.Message);
                }
            }
            else
            {
                throw new ArgumentException("Error. Context DB or entity cannot be null.");
            }
            return null;
        }
    }
}
