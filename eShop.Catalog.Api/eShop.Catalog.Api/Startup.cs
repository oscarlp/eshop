using eShop.Catalog.Api.Controllers;
using eShop.Catalog.Data.ContextDB;
using eShop.Catalog.Data.Factory;
using eShop.Catalog.Data.Mappers;
using eShop.Catalog.Data.Seeds;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace eShop.Catalog.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            /////////////////////////////////////////MY CONFIGURE SERVICE //////////////////////////////////////////////////////////
            // Add a CORS Policy to allow "Everything":
            services.AddCors(o =>
            {
                o.AddPolicy("Everything", p =>
                {
                    p.AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowAnyOrigin();
                });
            });

            // Register the Options:
            services.AddOptions();

            // Register Database Entity Maps:
            services.AddSingleton<IEntityTypeMap, CatalogItemMapper>();
            services.AddSingleton<IEntityTypeMap, CatalogTypeMapper>();
            services.AddSingleton<IEntityTypeMap, CatalogBrandMapper>();

            // Register the Seed:
            services.AddSingleton<IDbContextSeed, DbContextSeed>();

            //********************************CONNECTIONS**********************************
            //var connectionString = Environment.GetEnvironmentVariable("DB_CONNECT_SQL_SERVER");
            var dbContextOptions = new DbContextOptionsBuilder<catalogDBContext>()
                .UseSqlServer(Configuration.GetConnectionString("DockerSqlConnect"))
                .Options;
            //___________________________________________________________
            //*****************************************************************************

            services.AddSingleton(dbContextOptions);

            // Finally register the DbContextOptions:
            services.AddSingleton<catalogDBContextOptions>();

            // This Factory is used to create the DbContext from the custom DbContextOptions:
            services.AddSingleton<IContextDBFactory, ContextDBFactory>();

            // Finally Add the Applications DbContext:
            services.AddDbContext<catalogDBContext>();

            services
                // Use MVC:
                .AddMvc()
                // Add Application Modules:
                .AddApplicationPart(typeof(CatalogController).Assembly);

            services.AddSwaggerGen(options =>
            {
                options.DescribeAllEnumsAsStrings();
                options.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Title = "eShop - Catalog HTTP Api",
                    Version = "v1",
                    Description = "The catalog HTTP Api",
                });
            });

            ///////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            services.AddControllers();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseCors(policyName: "Everything");
            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseSwagger().UseSwaggerUI(e =>
            {
                e.SwaggerEndpoint("/swagger/v1/swagger.json", "Catalog.Api v1");
            });
        }
    }
}
