﻿using eShop.Catalog.Business;
using eShop.Catalog.Data.Factory;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace eShop.Catalog.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CatalogController : Controller
    {
        private readonly CatalogItemBL catalogItemBL;
        public CatalogController(IContextDBFactory contextDB)
        {
            this.catalogItemBL = new CatalogItemBL(contextDB);
        }

        [Route("getcatalog"), HttpGet]
        public async Task<ActionResult> GetCatalog()
        {
            try
            {
                var result = await catalogItemBL.GetAllItems();
                return new JsonResult(new
                {
                    response = true,
                    data = result,
                    error = ""
                });
            }
            catch (Exception ex)
            {
                return new JsonResult(new
                {
                    response = false,
                    data = false,
                    error = ex.Message
                });
            }
        }

        [Route("getcatalogfake"), HttpGet]
        public ActionResult GetCatalogFake()
        {
            try
            {
                return new JsonResult(new
                {
                    response = true,
                    data = "Fake data",
                    error = ""
                });
            }
            catch (Exception ex)
            {
                return new JsonResult(new
                {
                    response = false,
                    data = false,
                    error = ex.Message
                });
            }
        }
    }
}