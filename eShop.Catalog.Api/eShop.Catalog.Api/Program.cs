using eShop.Catalog.Data.ContextDB;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.IO;

namespace eShop.Catalog.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args)

                .Build();

            CreateDbIfNotExists(host);


            host.Run();
        }

        private static void CreateDbIfNotExists(IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;

                try
                {
                    var context = services.GetRequiredService<catalogDBContext>();
                   if( !(context.Database.GetService<IDatabaseCreator>() as RelationalDatabaseCreator).Exists())
                    {
                        context.Database.EnsureCreated();
                    }                
                }
                catch (Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occurred creating the DB.");

                    //EventLog eventLog = new EventLog();
                    //eventLog.Source = "eShop.Catalog.Api";
                    //eventLog.WriteEntry(ex.Message + "\n\n" + "Trace: " + ex.StackTrace, EventLogEntryType.Error);
                }
            }
        }

        // EF Core uses this method at design time to access the DbContext
        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
            => WebHost.CreateDefaultBuilder(args)
                .UseIISIntegration()
                .UseStartup<Startup>();
    }
}
