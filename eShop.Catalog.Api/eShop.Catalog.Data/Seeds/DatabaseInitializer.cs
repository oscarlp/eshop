﻿using eShop.Catalog.Data.ContextDB;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace eShop.Catalog.Data.Seeds
{
    public static class DatabaseInitializer
    {
        public static void Initializer(IServiceProvider provider)
        {
            using (var serviceScope = provider.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<catalogDBContext>();

                // auto migration
                context.Database.Migrate();

                // Seed the database.
                //InitializeUserAndRoles(context);
            }
        }
    }
}
