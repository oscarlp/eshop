﻿using Microsoft.EntityFrameworkCore;

namespace eShop.Catalog.Data.Seeds
{
    public interface IDbContextSeed
    {
        void Seed(ModelBuilder builder);
    }
}
