﻿using eShop.Catalog.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace eShop.Catalog.Data.Seeds
{
    public class DbContextSeed : IDbContextSeed
    {
        private readonly IConfiguration configuration;

        public DbContextSeed(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public void Seed(ModelBuilder builder)
        {
            if (builder != null)
            {
                // Add :
                var type1 = new CatalogType { Id = 1, Name = "Food" };
                var type2 = new CatalogType { Id = 2, Name = "Clothes" };
                var type3 = new CatalogType { Id = 3, Name = "Decorative" };

                //Add
                var brand1 = new CatalogBrand { Id = 1, Code = "0001", Name = "Coca Cola" };
                var brand2 = new CatalogBrand { Id = 2, Code = "0101", Name = "Nike" };
                var brand3 = new CatalogBrand { Id = 3, Code = "1001", Name = "Ikea" };

                //Add
                var item1 = new CatalogItem { Id = 1, BrandId = 1, Name = "Coca cola Zero", Description = "Coca cola zero from coca cola company", TypeId = 1, AvailableStock = 500, Price = 2000 };
                var item2 = new CatalogItem { Id = 2, BrandId = 2, Name = "Nike Jordan", Description = "Nike Jordan 2020", TypeId = 2, AvailableStock = 10, Price = 25000 };

                builder.Entity<CatalogType>().HasData(type1, type2, type3);
                builder.Entity<CatalogBrand>().HasData(brand1, brand2, brand3);
                builder.Entity<CatalogItem>().HasData(item1, item2);
            }

        }
    }
}
