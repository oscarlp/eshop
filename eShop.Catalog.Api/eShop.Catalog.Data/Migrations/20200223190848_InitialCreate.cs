﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace eShop.Catalog.Data.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BRAND",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Code = table.Column<string>(maxLength: 30, nullable: false),
                    Name = table.Column<string>(maxLength: 60, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BRAND", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TYPE",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(maxLength: 60, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TYPE", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ITEM",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(maxLength: 30, nullable: false),
                    Description = table.Column<string>(maxLength: 4000, nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    PictureFileName = table.Column<string>(maxLength: 200, nullable: true),
                    PictureUri = table.Column<string>(nullable: true),
                    TypeId = table.Column<int>(nullable: false),
                    BrandId = table.Column<int>(nullable: false),
                    AvailableStock = table.Column<int>(nullable: false),
                    RestockThreshold = table.Column<int>(nullable: false),
                    MaxStockThreshold = table.Column<int>(nullable: false),
                    OnReorder = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ITEM", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ITEM_BRAND_BrandId",
                        column: x => x.BrandId,
                        principalTable: "BRAND",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ITEM_TYPE_TypeId",
                        column: x => x.TypeId,
                        principalTable: "TYPE",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "BRAND",
                columns: new[] { "Id", "Code", "Name" },
                values: new object[,]
                {
                    { 1, "0001", "Coca Cola" },
                    { 2, "0101", "Nike" },
                    { 3, "1001", "Ikea" }
                });

            migrationBuilder.InsertData(
                table: "TYPE",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Food" },
                    { 2, "Clothes" },
                    { 3, "Decorative" }
                });

            migrationBuilder.InsertData(
                table: "ITEM",
                columns: new[] { "Id", "AvailableStock", "BrandId", "Description", "MaxStockThreshold", "Name", "OnReorder", "PictureFileName", "PictureUri", "Price", "RestockThreshold", "TypeId" },
                values: new object[,]
                {
                    { 1, 500, 1, "Coca cola zero from coca cola company", 0, "Coca cola Zero", false, null, null, 2000m, 0, 1 },
                    { 2, 10, 2, "Nike Jordan 2020", 0, "Nike Jordan", false, null, null, 25000m, 0, 2 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_BRAND_Code",
                table: "BRAND",
                column: "Code",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BRAND_Name",
                table: "BRAND",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ITEM_BrandId",
                table: "ITEM",
                column: "BrandId");

            migrationBuilder.CreateIndex(
                name: "IX_ITEM_Name",
                table: "ITEM",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ITEM_TypeId",
                table: "ITEM",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_TYPE_Name",
                table: "TYPE",
                column: "Name",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ITEM");

            migrationBuilder.DropTable(
                name: "BRAND");

            migrationBuilder.DropTable(
                name: "TYPE");
        }
    }
}
