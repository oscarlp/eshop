﻿using Microsoft.EntityFrameworkCore;

namespace eShop.Catalog.Data.Mappers
{
    public interface IEntityTypeMap
    {
        void Map(ModelBuilder builder);
    }
}
