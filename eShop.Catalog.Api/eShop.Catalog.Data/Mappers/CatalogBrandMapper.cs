﻿using eShop.Catalog.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace eShop.Catalog.Data.Mappers
{
    public class CatalogBrandMapper : BaseEntityMapper<CatalogBrand>
    {
        protected override void InternalMap(EntityTypeBuilder<CatalogBrand> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Code).IsRequired().HasMaxLength(30);
            builder.HasIndex(x => x.Code).IsUnique();
            builder.Property(x => x.Name).IsRequired().HasMaxLength(60);
            builder.HasIndex(x => x.Name).IsUnique();

            builder.ToTable("BRAND");
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
        }
    }
}
