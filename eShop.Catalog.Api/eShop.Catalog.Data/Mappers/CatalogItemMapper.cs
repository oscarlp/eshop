﻿using eShop.Catalog.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace eShop.Catalog.Data.Mappers
{
    public class CatalogItemMapper : BaseEntityMapper<CatalogItem>
    {
        protected override void InternalMap(EntityTypeBuilder<CatalogItem> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Name).IsRequired().HasMaxLength(30);
            builder.Property(x => x.Description).IsRequired().HasMaxLength(4000);
            builder.Property(x => x.PictureFileName).HasMaxLength(200);
            builder.Property(x => x.TypeId).IsRequired();
            builder.Property(x => x.BrandId).IsRequired();

            builder.HasIndex(x => x.Name).IsUnique();

            builder.ToTable("ITEM");
            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            //-------If you wanna apply especifics column names to dabase
            //builder.Property(x => x.Name).HasColumnName("Name");
            //builder.Property(x => x.Description).HasColumnName("Description");
            //builder.Property(x => x.).HasColumnName("PASSWORD");
        }
    }
}
