﻿using eShop.Catalog.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace eShop.Catalog.Data.Mappers
{
    public class CatalogTypeMapper : BaseEntityMapper<CatalogType>
    {
        protected override void InternalMap(EntityTypeBuilder<CatalogType> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Name).IsRequired().HasMaxLength(60);
            builder.HasIndex(x => x.Name).IsUnique();

            builder.ToTable("TYPE");
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
        }
    }
}
