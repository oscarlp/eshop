﻿using eShop.Catalog.Data.Mappers;
using eShop.Catalog.Data.Seeds;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace eShop.Catalog.Data.ContextDB
{
    public class catalogDBContextOptions
    {
        public readonly DbContextOptions<catalogDBContext> Options;
        public readonly IDbContextSeed DbContextSeed;
        public readonly IEnumerable<IEntityTypeMap> Mappings;

        public catalogDBContextOptions(DbContextOptions<catalogDBContext> options, IDbContextSeed dbContextSeed, IEnumerable<IEntityTypeMap> mappings)
        {
            DbContextSeed = dbContextSeed;
            Options = options;
            Mappings = mappings;
        }
    }

    public class catalogDBContext : DbContext
    {
        private readonly catalogDBContextOptions options;
        public catalogDBContext(catalogDBContextOptions options) : base(options.Options)
        { this.options = options; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            foreach (var mapping in options.Mappings)
            {
                mapping.Map(builder);
            }
            options.DbContextSeed?.Seed(builder);
        }

    }

    public static class catalogDBContextExtensions
    {
        public static DbSet<TEntityType> DbSet<TEntityType>(this catalogDBContext context)
            where TEntityType : class
        {
            return context.Set<TEntityType>();
        }
    }
}
