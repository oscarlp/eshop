﻿using eShop.Catalog.Data.ContextDB;

namespace eShop.Catalog.Data.Factory
{
    public interface IContextDBFactory
    {
        catalogDBContext Process();
    }
}
