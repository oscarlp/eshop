﻿using eShop.Catalog.Data.ContextDB;

namespace eShop.Catalog.Data.Factory
{
    public class ContextDBFactory : IContextDBFactory
    {
        private readonly catalogDBContextOptions options;

        public ContextDBFactory(catalogDBContextOptions options)
        {
            this.options = options;
        }
        public catalogDBContext Process()
        {
            return new catalogDBContext(options);
        }
    }
}
