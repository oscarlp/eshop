﻿using eShop.Catalog.Data.Factory;
using eShop.Catalog.Entities;
using eShop.Catalog.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eShop.Catalog.Business
{
    public class CatalogItemBL : RepositoryCatalog<CatalogItem>
    {
        private readonly IContextDBFactory contextDBFactory;
        public CatalogItemBL(IContextDBFactory contextDBFactory) : base(contextDBFactory)
        {
            this.contextDBFactory = contextDBFactory;
        }

        public async Task<List<CatalogItem>> GetAllItems()
        {
            try
            {
                return await this.List();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
