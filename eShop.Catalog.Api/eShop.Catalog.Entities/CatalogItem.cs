﻿using System.Runtime.Serialization;

namespace eShop.Catalog.Entities
{
    [DataContract]
    public class CatalogItem
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public decimal Price { get; set; }

        [DataMember]
        public string PictureFileName { get; set; }

        [DataMember]
        public string PictureUri { get; set; }

        [DataMember]
        public int TypeId { get; set; }
        public CatalogType Type { get; set; }

        [DataMember]
        public int BrandId { get; set; }
        public CatalogBrand Brand { get; set; }

        [DataMember]
        public int AvailableStock { get; set; }

        [DataMember]
        public int RestockThreshold { get; set; }

        [DataMember]
        public int MaxStockThreshold { get; set; }

        [DataMember]
        public bool OnReorder { get; set; }

    }
}
