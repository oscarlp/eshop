﻿using System.Runtime.Serialization;

namespace eShop.Catalog.Entities
{
    [DataContract]
    public class CatalogBrand
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
